/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {createBvTreeFromVertices, createBvTreeFromEntries, createSubDomain} = require('../../../src/classes/bvTree/creator');
const BvTree = require('../../../src/classes/bvTree/tree');
const {Domain, DomainEntry} = require('../../../src/math/domain');
const Vector3 = require('../../../src/math/vector3');

const EMPTY_DELTA = new Vector3(0.0);

describe("bvTree", () => 
{
    describe("createBvTreeFromEntries", () => {

        test('empty entry-array', () => {
            expect(() => createBvTreeFromEntries([], 0.0))
                .toThrow("Error: BvTree creation needs at least one child class");
        });

        test('single entry', () => {
            const tree = createBvTreeFromEntries([
                {
                    sizeHalf: [1.0, 2.0, 3.0],
                    translate: [-2.0, 0.0, -1.0],
                }
            ], EMPTY_DELTA);

            expect(tree).toEqual(
                new DomainEntry(new Domain([-3, -2, -4], [-1, 2, 2]), 0)
            );
        });

        test('multiple entries', () => {
            const tree = createBvTreeFromEntries([
                {
                    sizeHalf: [1.0, 2.0, 3.0],
                    translate: [-2.0, 0.0, -1.0],
                },
                {
                    sizeHalf: [5.0, 0.4, 1.0],
                    translate: [-1.0, 1.0, 1.0],
                    children: [
                        {
                            sizeHalf: [5.0, 3.0, 1.0],
                            translate: [2.0, -5.0, -1.0],
                        }
                    ]
                }
            ], EMPTY_DELTA);

            // -3.6 and -4.4 is due to correct rounding to prevent to tight domains
            expect(tree).toEqual(
                new BvTree(
                    new Domain([-4, -4.4, -4], [6, 2, 2]),
                    new DomainEntry(new Domain([-3, -2, -4], [-1, 2, 2]), 0),
                    new DomainEntry(new Domain( [-4, -4.4, -1], [6, -3.6, 1]), 1),
                )
            );
        });
    });

    describe("createBvTreeFromVertices", () => {

        test('empty polygon-array', () => {
            expect(() => createBvTreeFromVertices([], [], 0.0))
                .toThrow("Error: BvTree creation needs at least one polygon");
        });
    
        test('single polygon', () => {
            const tree = createBvTreeFromVertices([
                [0.0, 0.0, 0.0],
                [1.0, 0.0, 0.0],
                [1.0, 1.0, 0.0]
            ], [
                [0,1,2]
            ], 0.0, 0.0);

            expect(tree).toEqual(
                new DomainEntry(new Domain([0, 0, 0], [1, 1, 0]), 0)
            );
        });

        test('polygons, n=2', () => {
            const tree = createBvTreeFromVertices([
                [ 0.0, 0.0,  1.0],
                [-1.0, 0.0,  1.0],
                [ 1.0, 1.0, -1.0],
                [ 0.0, 1.0,  1.0]
            ], [
                [0,1,3],
                [3,2,0],
            ], 0.0, 0.0);

            expect(tree).toEqual(
                new BvTree(
                    new Domain([-1, 0, -1], [ 1, 1, 1]),
                    new DomainEntry(
                        new Domain([-1, 0, 1], [ 0, 1, 1]), 0
                    ),
                    new DomainEntry(
                        new Domain([0, 0, -1], [1, 1, 1]), 1
                    )
                )
            );
        });

/**
 * Prints a tree, used for debugging
 * @param {BvTree} tree 
 * @param {String} level 
 */
function treeToString(tree, level = "")
{
    let str = level + `new Domain([${tree.domain.min.join(", ")}], [${tree.domain.max.join(", ")}])`;
    if(tree.index >= 0) {
        str += "," + tree.index;
    }
    str += "\n";

    if(tree.childA && tree.childB) {
        str += treeToString(tree.childA, level + "    ");
        str += treeToString(tree.childB, level + "    ");
    }

    return str;
}

        test('polygons, n=5', () => {
            const tree = createBvTreeFromVertices([
                [ 0.0, 0.0,  5.0],
                [-5.0, 0.0,  5.0],
                [ 5.0, 5.0, -5.0],
                [ 0.0, 5.0,  5.0],
                [ 0.0,-5.0, -5.0],
            ], [
                [0,1,3], [3,2,0],
                [2,1,0], [1,2,0],
                [0,3,2],
            ], 0.0, 0.0);

            expect(tree).toEqual(
                new BvTree(
                    new Domain([-5, 0, -5], [5, 5, 5]),
                    new BvTree(
                        new Domain([-5, 0, -5], [5, 5, 5]),
                        new DomainEntry(new Domain([-5, 0, 5], [0, 5, 5]),  0),
                        new DomainEntry(new Domain([-5, 0, -5], [5, 5, 5]), 2)
                    ),
                    new BvTree(
                        new Domain([-5, 0, -5], [5, 5, 5]),
                        new DomainEntry(new Domain([-5, 0, -5], [5, 5, 5]), 3),
                        new BvTree(
                            new Domain([0, 0, -5], [5, 5, 5]),
                            new DomainEntry(new Domain([0, 0, -5], [5, 5, 5]), 1),
                            new DomainEntry(new Domain([0, 0, -5], [5, 5, 5]), 4)
                        )
                    )
                )
            );
        });
    });

    describe("createSubDomain", () => {

        test('sub-domain without parent-domain', () => {
            const data = createSubDomain(
                undefined,
            {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            });
            expect(data).toEqual([0x00, 0x00, 0x00]);
        });

        test('sub-domain with same size', () => {
            const data = createSubDomain({
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            },{
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            });
            // data is the distance to the old box, so it's zero
            expect(data).toEqual([0x00, 0x00, 0x00]);
        });

        test('sub-domain with smallest size (technically wrong data)', () => {
            const data = createSubDomain({
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            },{
                min: [ 1.0,  1.0,  1.0],
                max: [-1.0, -1.0, -1.0]
            });
            expect(data).toEqual([0xFF, 0xFF, 0xFF]);
        });

        test('sub-domain axis check', () => {
            const domain = {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            };

            expect(createSubDomain(domain,{
                min: [-1.0,  1.0,  1.0],
                max: [+1.0, -1.0, -1.0]
            })).toEqual([0x00, 0xFF, 0xFF]); // X

            expect(createSubDomain(domain,{
                min: [ 1.0, -1.0,  1.0],
                max: [-1.0, +1.0, -1.0]
            })).toEqual([0xFF, 0x00, 0xFF]); // Y

            expect(createSubDomain(domain,{
                min: [ 1.0, 1.0, -1.0],
                max: [-1.0, -1.0, +1.0]
            })).toEqual([0xFF, 0xFF, 0x00]); // Z
        });

        test('sub-domain min/max check', () => {
            const domain = {
                min: [-1.0, -1.0, -1.0],
                max: [ 1.0,  1.0,  1.0]
            };

            expect(createSubDomain(domain,{
                min: [+1.0, +1.0, +1.0],
                max: [ 1.0,  1.0,  1.0]
            })).toEqual([0xF0, 0xF0, 0xF0]); // Min

            expect(createSubDomain(domain,{
                min: [-1.0, -1.0, -1.0],
                max: [-1.0, -1.0, -1.0]
            })).toEqual([0x0F, 0x0F, 0x0F]); // Max
        });

        test('sub-domain with test values (1)', () => {
            expect(createSubDomain({
                min: [-1.247452, 3.540257, -3.706786],
                max: [ 5.124335, 6.708972,  1.382037]
            }, {
                min: [0.134041, 3.540257, -1.882916],
                max: [5.124335, 6.582785,  1.179384]
            })).toEqual([
                0x60, 0x02, 0x83
            ]);
        });

        test('sub-domain with test values (2)', () => {
            expect(createSubDomain({
                min: [3.263550, 5.958744, -1.829768],
                max: [5.399019, 6.885951, -1.036530]
            }, {
                min: [4.860428, 5.962847, -1.826258],
                max: [5.389570, 6.475682, -1.461228]
            })).toEqual([
                0xD1, 0x1A, 0x1A
            ]);
        });

        test('sub-domain with test values, rounding', () => {
            expect(createSubDomain({
                min: [3.165313, 4.860302, -1.828716],
                max: [5.124335, 6.517855, -0.285255]
            }, {
                min: [3.304004, 4.867637, -1.494072],
                max: [5.115667, 6.517855, -0.312573]

            // this should not be 0x41 and 0x72, otherwise the rounding creates domains that are too small
            })).toEqual([
                0x30, 0x10, 0x62
            ]);
        });
    });
});  
