/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {readDataRuns, writeDataRuns, createDataRuns} = require('../../../src/classes/compressedMesh/dataRun');
const BinaryFile = require('../../../src/binaryFile');

describe("compressedMesh", () => 
{
    describe("readDataRuns", () => 
    {
        test('data runs', () => {
            const testfile = new BinaryFile(Buffer.from([
                0x46, 0x36, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 
                0x4E, 0x8F, 0x01, 0x00, 0x01 ,0x01, 0x00, 0x00,
                0x31, 0x96, 0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 
                0x42, 0x16, 0x00, 0x00, 0x03 ,0x02, 0x00, 0x00,
                0x42, 0xD1, 0x00, 0x00, 0x05, 0x01, 0x00, 0x00, 
                0x5E, 0x8F, 0x01, 0x00, 0x06 ,0x01, 0x00, 0x00,
            ]));

            const indices = readDataRuns(testfile, 6);
            expect(indices).toEqual([
                {data: 0x4636, materialIndex: 0, count: 1}, 
                {data: 0x4E8F, materialIndex: 1, count: 1},
                {data: 0x3196, materialIndex: 0, count: 1}, 
                {data: 0x4216, materialIndex: 0, count: 2},
                {data: 0x42D1, materialIndex: 0, count: 1}, 
                {data: 0x5E8F, materialIndex: 1, count: 1},
            ]);
        });
    });

    describe("writeDataRuns", () => 
    {
        test('data runs', () => {
            const testfile = new BinaryFile();
            writeDataRuns(testfile, [
                {data: 0x4636, materialIndex: 0, count: 1}, 
                {data: 0x4E8F, materialIndex: 1, count: 1},
                {data: 0x3196, materialIndex: 0, count: 1}, 
                {data: 0x4216, materialIndex: 0, count: 2},
                {data: 0x42D1, materialIndex: 0, count: 1}, 
                {data: 0x5E8F, materialIndex: 1, count: 1},
            ]);

            expect(testfile.getBuffer()).toEqual(Buffer.from([
                0x46, 0x36, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 
                0x4E, 0x8F, 0x01, 0x00, 0x01 ,0x01, 0x00, 0x00,
                0x31, 0x96, 0x00, 0x00, 0x02, 0x01, 0x00, 0x00, 
                0x42, 0x16, 0x00, 0x00, 0x03 ,0x02, 0x00, 0x00,
                0x42, 0xD1, 0x00, 0x00, 0x05, 0x01, 0x00, 0x00, 
                0x5E, 0x8F, 0x01, 0x00, 0x06 ,0x01, 0x00, 0x00,
            ]));
        });
    });

    describe("createDataRuns", () => 
    {
        test('empty indices', () => {
            const dataRuns = createDataRuns([]);
            expect(dataRuns).toEqual([]);
        });

        test('single index pair', () => {
            const dataRuns = createDataRuns([
                [0,1,2]
            ]);
            expect(dataRuns).toEqual([
                {data: 0x59F6, materialIndex: 0, count: 1},
            ]);
        });

        test('single indices', () => {
            const dataRuns = createDataRuns([
                [0,1,2], [2,3,4], [4,5,6], [6,7,8,9]
            ]);
            expect(dataRuns).toEqual([
                {data: 0x59F6, materialIndex: 0, count: 1},
                {data: 0x59F6, materialIndex: 0, count: 1},
                {data: 0x59F6, materialIndex: 0, count: 1},
                {data: 0x59F6, materialIndex: 0, count: 1},
            ]);
        });
    });
});