/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const fs = require("fs-extra");

const generateFile = require("../../src/generateFile");
const parseFile = require("../../src/parseFile")
const testFiles = require("./testFiles");

function loadBuffer(fileInput)
{
    return fs.readFileSync("test/integration/files/" + fileInput);
}

describe('Integration-Test', () => {

    describe('Error handling', () => 
    {
        test('Invalid version', () => {
            const collData = {
                version: 0
            };
            const error = "Incompatible file version, make sure the version is '4'";
            expect(() => generateFile(collData)).toThrowError(error);
        });

        test('Unknown class', () => {
            console.error = () => {};
            const collData = {
                version: 4,
                data: {
                    type: "hkRootLevelContainer",
                    name: "Physics Data",
                    physicsClass: "hkpPhysicsData",
                    children: [{
                        type: "hkLieberDiePestInDerHandAlsDieCholeraAufDemDach",
                        children: []
                    }]
                }
            };
            const error = "Unknown class 'hkLieberDiePestInDerHandAlsDieCholeraAufDemDach'";
            expect(() => new generateFile(collData)).toThrowError(error);
        });
    });
    
    describe("Write files", () => {
        testFiles.forEach(testCase => 
        {
            if(!testCase.write)return;

            describe(testCase.name, () => {
                for(let file in testCase.files) 
                {
                    test(testCase.files[file], () => {
                        const collData = fs.readJSONSync("test/integration/files/" + file + ".json");
                        const createdBuffer = generateFile(collData);
                        const testBuffer = loadBuffer(file + ".bin");
                        expect(createdBuffer).toEqualBinary(testBuffer);
                    });   
                }
            });    
        });
    });

    describe("Read files", () => {
        testFiles.forEach(testCase => 
        {
            if(!testCase.read)return;

            describe(testCase.name, () => {
                for(let file in testCase.files) 
                {
                    test(testCase.files[file], () => {
                        const collBuffer = loadBuffer(file + ".bin");
                        const createdJson = parseFile(collBuffer, testCase.extension);
                        const collData = fs.readJSONSync("test/integration/files/" + file + ".json");

                        // @TODO workaround for missing compound info reader (issue-1)
                        delete collData.compound;

                        try {
                            expect(createdJson).toMatchCloseTo(collData, 4);
                        } catch(e) {
                            console.log(JSON.stringify(createdJson, null, 2));
                            throw e;
                        }
                    });   
                }
            });    
        });
    });
});