/**
* @copyright 2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" this.file in the root directory
*/

const {exec} = require('child_process');
const uuidv4 = require('uuid/v4');
const fs = require('fs-extra');
const path = require('path');
const decodeYaz0 = require('../../src/yaz0/decode');
const testFiles = require("./testFiles");

async function getExecResult(tool, params)
{
    return new Promise((resolve, reject) => 
    {
        exec(`node cli/${tool}.js ${params}`, 
            (err, stdout, stderr) => err ? reject(err) : resolve(stdout)
        );
    });
}

describe('CLI Integration-Test', () => {

    describe('Create', () => 
    {
        test('non existing file', () => {
            const testCall = getExecResult("create", "i-do-not-exist");
            expect(testCall).rejects.toThrow();
        });

        test('test-file', async () => {
            const createdFilePath = path.join('test', 'integration', 'tmp', uuidv4() + ".hkrb");
            await getExecResult("create", "./test/integration/files/model-box-8.json " + createdFilePath);

            const createdBuffer = await fs.readFile(createdFilePath);
            const testBuffer = await fs.readFile(path.join('test', 'integration', 'files', 'model-box-8.bin'));

            expect(createdBuffer).toEqualBinary(testBuffer);
        });


        describe("test files with yaz0", () => {
            testFiles.forEach(testCase => 
            {
                if(!testCase.write)return;
    
                describe(testCase.name, () => {
                    for(let file in testCase.files) 
                    {
                        test(testCase.files[file], async () => {
                            const createdFilePath = path.join('test', 'integration', 'tmp', uuidv4() + ".shkrb");
                            await getExecResult("create", "-y ./test/integration/files/" + file + ".json " + createdFilePath);

                            const createdBufferYaz0 = await fs.readFile(createdFilePath);
                            expect(createdBufferYaz0.slice(0,4).toString("utf8")).toEqual("Yaz0");

                            const createdBuffer = decodeYaz0(createdBufferYaz0);

                            const testBuffer = await fs.readFile(path.join('test', 'integration', 'files', file + '.bin'));

                            expect(createdBuffer).toEqualBinary(testBuffer);
                        });   
                    }
                });    
            });
        });
    });

    describe('Parse', () => 
    {
        test('non existing file', () => {
            const testCall = getExecResult("parse", "i-do-not-exist");
            expect(testCall).rejects.toThrow();
        });

        test('test-file', async () => {
            const createdFilePath = path.join('test', 'integration', 'tmp', uuidv4() + ".json");
            await getExecResult("parse", "./test/integration/files/model-single-box.bin " + createdFilePath);

            const createdJson = await fs.readJSON(createdFilePath);
            const testJson = await fs.readJSON(path.join('test', 'integration', 'files', 'model-single-box.json'));

            // workaround for the extra attributes, reader is still WIP
            delete createdJson.data.children[0].children[0].children[0].children[0].children[0].radius;

            expect(createdJson).toEqual(testJson);
        });
    });

    describe('Import', () => 
    {
        test('non existing file', () => {
            const testCall = getExecResult("import", "-c i-do-not-exist.obj");
            expect(testCall).rejects.toThrow();
        });

        test('test-file', async () => {
            const createdFilePath = path.join('test', 'integration', 'tmp', uuidv4() + ".json");
            await getExecResult("import", "-c ./test/integration/files/multi-obj-norm.obj " + createdFilePath);

            const createdJson = await fs.readJSON(createdFilePath);
            const testJson = await fs.readJSON(path.join('test', 'integration', 'files', 'multi-obj-norm.json'));

            expect(createdJson).toEqual(testJson);
        });
        
    });
});