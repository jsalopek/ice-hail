const helpers = require('../../cli/helpers');

describe('CLI Integration-Test', () => {

  describe('Node Check', () => {
    test('should not throw error when version matches', () => {
      const versionCheckResult = helpers.nodeCheck.checkVersion(false);
      expect(versionCheckResult).toBeTruthy();
    });

    test('should throw error when version differs', () => {
      expect(() => { helpers.nodeCheck.checkVersion(true) }).toThrow();
    });
  });

});