/**
* @copyright 2018  2019 - Max Bebök
* @author Max Bebök
* @license GNU-GPLv3 - see the "LICENSE" file in the root directory
*
* Ported over from Ice-Spear, only used in tests for now
*/

const BinaryFile = require('../binaryFile');

class YAZ0
{
    constructor()
    {
        this.file = null;
        this.maxSize = 100 * 1024 * 1024; // 100 MB

        this.bufferOut = null;
        this.bufferOutPos = 0;

        this.chunks = Buffer.from([0,0,0]);
    }

    writeOut(valOrBuffer)
    {
        if(valOrBuffer instanceof Buffer)
        {
            valOrBuffer.copy(this.bufferOut, this.bufferOutPos);
            this.bufferOutPos += valOrBuffer.length;
        }else{
            this.bufferOut[this.bufferOutPos++] = valOrBuffer;
        }
    }

    parseBlock()
    {
        let header = this.file.read("u8");

        for(let i=7; i>=0; --i)
        {
            if(this.bufferOutPos >= this.bufferOut.length)
                return false;

            let chunkType = (header >> i) & 1;
            if(chunkType == 1)
            {
                this.writeOut(this.file.read("u8"));
            }else{

                let length = 0;

                this.chunks[0] = this.file.read("u8");
                this.chunks[1] = this.file.read("u8");

                let offset = (((this.chunks[0] & 0xF) << 8) | this.chunks[1]) + 1;

                if(((this.chunks[0] >> 4) & 0xF) == 0) // has 3 bytes
                {
                    this.chunks[2] = this.file.read("u8");
                    length = this.chunks[2] + 0x12;
                }else{
                    length = ((this.chunks[0] >> 4) & 0xF) + 0x02;
                }

                for(let i=0; i<length; ++i)
                {
                    this.writeOut(this.bufferOut[this.bufferOutPos - offset]);
                }
            }
        }

        return true;
    }

    decode(buffer)
    {
        this.file = new BinaryFile(buffer);
        this.file.setEndian("big");

        const magic = this.file.readString(4);
        if(magic != "Yaz0") {
            return false;
        }

        const size = this.file.read("u32");
        const offset = this.file.read("u32");
        const flags = this.file.read("u32");

        if(size > this.maxSize)
        {
            console.error("Yaz0: maximum file size: " + this.maxSize);
            return false;
        }

        this.bufferOut = Buffer.alloc(size);

        while(this.parseBlock());

        return this.bufferOut;
    }
};

module.exports = yaz0File => {
    const yaz0 = new YAZ0();
    return yaz0.decode(yaz0File);
};